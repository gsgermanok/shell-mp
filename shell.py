#!/usr/bin/env python3
#_*_ coding: cp1252_*_

import sys
print(sys.version)
import os

def launch(string, attr):
    if attr:
        print("\x1B[1m" + string +"\x1B[13m")
    else:
        print(string)

def  ls(attr):
    stat = os.stat(attr)
    if stat[0] == 32768:
        launch("- " + attr + " " + " ".join(str(v) for v in stat), False)
    try:
        items = list(os.ilistdir(attr))
        for line in items:
            launch((("d " if line[1] == 16384  else "- ") + line[0]), True if line[3] == 0 else False)
        return 1
    except:
        return 0

def rm(attr):
    stat = os.stat(attr)
    if stat[0] == 32768:
        launch("deleted - " + attr + " " + " ".join(str(v) for v in stat), False)
    os.remove(attr)
    return 1
    
def cat(attr):
    data = open(os.getcwd() + "/" + attr, 'r')
    print("«" + os.getcwd() + "/" + attr + "»")
    print(data.read())
    return 1

def touch(attr):
    file = open(attr, 'x')
    file.close()
    return 1

# Begin Directory section
# this section is for create / delete / change and obtain current folder
def mkdir(attr):
    os.mkdir(attr)
    return 1

def rmdir(attr):
    os.rmdir(attr)
    return 1

def cd(attr):
    os.chdir(attr)
    return 1

def pwd(attr):
    print(os.getcwd())
    return 1

def tail(attr):
    lines = 0
    params = attr.split()
    param = params.pop()
    if param == '-n':
        lines = params.pop()
    else:
        lines = 10

    files = open(params.pop(), 'r')
    ntuple = files.readline()
    print(">> ", ntuple, " <<") 
    for index in xrange(10):
        print(ntuple.pop())
    return 1

while True:
    inp = input('shell> ')
    if inp == 'exit':
        break

    if inp:
        inp = inp.split()
        inp.reverse()
        if inp.__class__ == 'str':
            command = inp
            attributes = "."
        else:
            command = inp.pop()
            attributes = inp
            attributes = " ".join(attributes) 

        try:
            exec(command + "(\"" + attributes + "\")")
        except:
            pass


